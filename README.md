590 Science White Papers have been submitted to the [Astro2020 Decadal Survey](http://sites.nationalacademies.org/SSB/CurrentProjects/SSB_185159) and a [csv file of info on them is available](https://sites.nationalacademies.org/DEPS/Astro2020/DEPS_192906) at that site. I took that list and created this [searchable HTML page](https://sciencelab.gitlab.io/astro2020/) with links to the PDF files for each paper.

What's searchable is the paper title, the principal author and their institution, the title, and a short description of the paper.

I created this because many of these papers are not available on the arXiv preprint server [[arXiv astro2020 papers query]](https://arxiv.org/search/advanced?advanced=1&terms-0-operator=AND&terms-0-term=astro2020&terms-0-field=all&terms-1-operator=OR&terms-1-term=astro_2020&terms-1-field=all&terms-2-operator=OR&terms-2-term=astro-2020&terms-2-field=all&terms-3-operator=OR&terms-3-term=astro+2020&terms-3-field=title&classification-physics=y&classification-physics_archives=all&classification-include_cross_list=include&date-filter_by=all_dates&date-year=&date-from_date=&date-to_date=&date-date_type=submitted_date&abstracts=hide&size=50&order=-announced_date_first) and I wanted to see what else was available.

The webpage is coded with simple html and javascript (javascript must be enabled to view the data). The webpage search input box shows the syntax:

For AND search, use \*: keyword1*keyword2

For OR search, use |: keyword1|keyword2

For NOT, use -- after 1st keyword: keyword1--keyword2

Astrobites has an article on the survey: https://astrobites.org/2019/03/22/astro2020_getting_underway

In addition to the Science White Papers which were due by March 11,2019, there is also a call for APC (Activities, Projects, or State of the Profession Consideration) White Papers due July 10, 2019. I will also create an app for those papers. More details about the APC White Papers are at the [Astro2020 Decadal Survey site](https://sites.nationalacademies.org/DEPS/Astro2020/DEPS_192906).

